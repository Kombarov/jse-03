package ru.kombarov.tm;

import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.enumerated.TerminalCommand;
import ru.kombarov.tm.executor.CommandExecutor;
import ru.kombarov.tm.service.ProjectService;
import ru.kombarov.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    public static void main(String[] args) throws IOException {

        System.out.println("*** WELCOME TO TASK MANAGER ***");

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        CommandExecutor commandExecutor = new CommandExecutor(br);

        while (true) {
            TerminalCommand terminalCommand = TerminalCommand.EMPTY;
            String input = br.readLine();
            terminalCommand = TerminalCommand.valueOf(input.toUpperCase());
            commandExecutor.processCommand(terminalCommand);
        }
    }
}
