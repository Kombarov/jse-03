package ru.kombarov.tm.executor;

import ru.kombarov.tm.enumerated.TerminalCommand;
import ru.kombarov.tm.service.ProjectService;
import ru.kombarov.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;

public class CommandExecutor {
    public TaskService taskService;
    public ProjectService projectService;

    public CommandExecutor(BufferedReader br) {
        this.projectService = new ProjectService(br);
        this.taskService = new TaskService(projectService, br);
    }

    public void processCommand(TerminalCommand terminalCommand) throws IOException {
        switch (terminalCommand) {
            case HELP:
                help();
                break;
            case PROJECT_CLEAR:
                projectService.projectClear();
                break;
            case PROJECT_CREATE:
                projectService.projectCreate();
                break;
            case PROJECT_LIST:
                projectService.projectList();
                break;
            case PROJECT_EDIT:
                projectService.projectEdit();
                break;
            case PROJECT_SELECT:
                projectService.projectSelect();
                break;
            case PROJECT_REMOVE:
                projectService.projectRemove();
                break;
            case TASK_CLEAR:
                taskService.taskClear();
                break;
            case TASK_CREATE:
                taskService.taskCreate();
                break;
            case TASK_LIST:
                taskService.taskList();
                break;
            case TASK_EDIT:
                taskService.taskEdit();
                break;
            case TASK_SELECT:
                taskService.taskSelect();
                break;
            case TASK_SHOW_BY_PROJECT:
                taskService.showTasksByProject();
                break;
            case TASK_ATTACH:
                taskService.taskAttach();
                break;
            case TASK_UNATTACH:
                taskService.taskUnattach();
                break;
            case TASK_REMOVE:
                taskService.taskRemove();
                break;
            case EXIT:
                projectService.br.close();
                taskService.br.close();
                System.exit(1);
                break;
        }
    }

    public void help() {
        String[] helplist = {   "help: Show all commands.",
                "project_clear: Remove all projects.",
                "project_create: Create new project.",
                "project_list: Show all projects.",
                "project_edit: Edit selected project.",
                "project_remove: Remove selected project.",
                "task_clear: Remove all tasks.",
                "task_create: Create new task.",
                "task_list: Show all tasks.",
                "task_edit: Edit selected task.",
                "task_remove: Remove selected task."};

        for (String string:helplist) System.out.println(string);
    }
}
