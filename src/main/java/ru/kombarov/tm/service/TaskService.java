package ru.kombarov.tm.service;

import ru.kombarov.tm.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ru.kombarov.tm.service.ProjectService.printProjects;
import static ru.kombarov.tm.util.DateUtil.parseDateFromString;
import static ru.kombarov.tm.util.DateUtil.parseDateToString;

public class TaskService {
    public List<Task> tasklist = new ArrayList<>();
    public ProjectService projectService;
    public BufferedReader br;

    public TaskService(ProjectService projectService, BufferedReader br) {
        this.projectService = projectService;
        this.br = br;
    }

    public void taskClear() {
        tasklist.clear();
        System.out.println("[ALL TASKS REMOVED]");
    }

    public void taskCreate() throws IOException {
        System.out.println("ENTER TASK NAME");
        Task task = new Task(br.readLine());
        System.out.println("ENTER DESCRIPTION");
        task.setDescription(br.readLine());
        System.out.println("ENTER START DATE");
        task.setStartDate(parseDateFromString(br.readLine()));
        System.out.println("ENTER END DATE");
        task.setEndDate(parseDateFromString(br.readLine()));
        tasklist.add(task);
        System.out.println("[OK]");
    }

    public void taskList() {
        for (int i=0; i<tasklist.size();i++) {
            System.out.println(i+1 + ". " + tasklist.get(i).getName());
        }
    }

    public void taskSelect() throws IOException {
        System.out.println("[TASK SELECT");
        System.out.println("ENTER TASK ID");
        for (int i=0; i<tasklist.size();i++) {
            System.out.println(i+1 + ". " + tasklist.get(i).getName());
        }
        final int taskId = Integer.parseInt(br.readLine()) - 1;
        Task task = tasklist.get(taskId);
        StringBuilder formatedTask = new StringBuilder();
        formatedTask.append("task name: ");
        formatedTask.append(task.getName());
        formatedTask.append("\ntask description: ");
        formatedTask.append(task.getDescription());
        formatedTask.append("\nstart date: ");
        formatedTask.append(parseDateToString(task.getStartDate()));
        formatedTask.append("\nend date: ");
        formatedTask.append(parseDateToString(task.getEndDate()));
        System.out.println(formatedTask);
    }

    public void showTasksByProject() throws IOException {
        System.out.println("[TASK SHOW BY PROJECT");
        System.out.println("ENTER PROJECT ID");
        final int projectId = Integer.parseInt(br.readLine()) - 1;
        String projectUUID = projectService.getProject(projectId).getId();
        List<Task> tasksByProject = new ArrayList<>();
        for(Task task : tasklist) {
            if(task.getProjectId().equals(projectUUID)) tasksByProject.add(task);
        }
        for (int i=0; i<tasksByProject.size();i++) {
            System.out.println(i+1 + ". " + tasksByProject.get(i).getName());
        }
    }

    public void taskEdit() throws IOException {
        System.out.println("[TASK EDIT]");
        System.out.println("ENTER TASK ID");
        for (int i=0; i<tasklist.size();i++) {
            System.out.println(i+1 + ". " + tasklist.get(i).getName());
        }
        final int taskId = Integer.parseInt(br.readLine()) - 1;
        System.out.println("ENTER NAME");
        Task task = tasklist.get(taskId);
        task.setName(br.readLine());
        System.out.println("ENTER DESCRIPTION");
        task.setDescription(br.readLine());
        System.out.println("ENTER START DATE");
        task.setStartDate(parseDateFromString(br.readLine()));
        System.out.println("ENTER END DATE");
        task.setEndDate(parseDateFromString(br.readLine()));
    }

    public void taskAttach() throws IOException {
        System.out.println("[TASK ATTACH]");
        System.out.println("ENTER TASK ID");
        for (int i=0; i<tasklist.size();i++) {
            System.out.println(i+1 + ". " + tasklist.get(i).getName());
        }
        final int taskId = Integer.parseInt(br.readLine()) - 1;
        Task task = tasklist.get(taskId);
        System.out.println("ENTER PROJECT ID");
        printProjects(projectService.getProjects());
        final int projectId = Integer.parseInt(br.readLine()) - 1;
        String projectUUID = projectService.getProject(projectId).getId();
        task.setProjectId(projectUUID);
    }

    public void taskUnattach() throws IOException {
        System.out.println("[TASK UNATTACH]");
        System.out.println("ENTER TASK ID");
        for (int i=0; i<tasklist.size();i++) {
            System.out.println(i+1 + ". " + tasklist.get(i).getName());
        }
        final int taskId = Integer.parseInt(br.readLine()) - 1;
        Task task = tasklist.get(taskId);
        task.setProjectId(null);
    }

    public void taskRemove() throws IOException {
        System.out.println("[TASK REMOVE]");
        System.out.println("ENTER TASK ID");
        for (int i=0; i<tasklist.size();i++) {
            System.out.println(i+1 + ". " + tasklist.get(i).getName());
        }
        final int taskId = Integer.parseInt(br.readLine()) - 1;
        tasklist.remove(taskId);
        System.out.println("[TASK REMOVED]");
    }
}
