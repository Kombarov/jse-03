package ru.kombarov.tm.service;

import ru.kombarov.tm.entity.Project;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ru.kombarov.tm.util.DateUtil.parseDateFromString;
import static ru.kombarov.tm.util.DateUtil.parseDateToString;

public class ProjectService {

    public List<Project> projectlist = new ArrayList<>();
    public BufferedReader br;

    public ProjectService(BufferedReader br) {
        this.br = br;
    }

    public List<Project> getProjects() {
        return projectlist;
    }

    public Project getProject(int projectId) {
        return projectlist.get(projectId);
    }

    public void projectClear() {
        projectlist.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    public void projectCreate() throws IOException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME");
        Project project = new Project(br.readLine());
        System.out.println("ENTER DESCRIPTION");
        project.setDescription(br.readLine());
        System.out.println("ENTER START DATE");
        project.setStartDate(parseDateFromString(br.readLine()));
        System.out.println("ENTER END DATE");
        project.setEndDate(parseDateFromString(br.readLine()));
        projectlist.add(project);
        System.out.println("[OK]");
    }

    public void projectList() {
        System.out.println("[PROJECT LIST]");
        printProjects(projectlist);
    }

    public void projectSelect() throws IOException {
        System.out.println("[PROJECT SELECT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectlist);
        final int projectId = Integer.parseInt(br.readLine()) - 1;
        printProject(projectlist.get(projectId));
    }

    public void projectEdit() throws IOException {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectlist);
        final int projectId = Integer.parseInt(br.readLine()) - 1;
        Project project = projectlist.get(projectId);
        System.out.println("ENTER NAME");
        project.setName(br.readLine());
        System.out.println("ENTER DESCRIPTION");
        project.setDescription(br.readLine());
        System.out.println("ENTER START DATE");
        project.setStartDate(parseDateFromString(br.readLine()));
        System.out.println("ENTER END DATE");
        project.setEndDate(parseDateFromString(br.readLine()));
    }

    public void projectRemove() throws IOException {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("ENTER PROJECT ID");
        printProjects(projectlist);
        final int projectId = Integer.parseInt(br.readLine()) - 1;
        projectlist.remove(projectId);
        System.out.println("[PROJECT REMOVED]");
    }

    public static void printProjects(List<Project> projects) {
        for (int i = 0; i < projects.size(); i++) {
            final Project project = projects.get(i);
            System.out.println((i + 1) + ". " + project.getName());
        }
    }

    private static void printProject(Project project) {
        StringBuilder formatedProject = new StringBuilder();
        formatedProject.append("project name: ");
        formatedProject.append(project.getName());
        formatedProject.append("\nproject description: ");
        formatedProject.append(project.getDescription());
        formatedProject.append("\nstart date: ");
        formatedProject.append(parseDateToString(project.getStartDate()));
        formatedProject.append("\nend date: ");
        formatedProject.append(parseDateToString(project.getEndDate()));
        System.out.println(formatedProject);
    }
}
